import { createExpressMiddleware } from "@trpc/server/adapters/express";
import cors from "cors";
import express from "express";
import path from "path";
import { trpcRouter } from "./routers";

const PORT = 3000;

const app = express();
const appRouter = express.Router();

app.use(cors());

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "../..", "client", "dist")));
}

app.use("/api", appRouter);
app.use(
  "/trpc",
  createExpressMiddleware({
    router: trpcRouter,
  })
);

appRouter.get("/", (req, res) => {
  res.send("Hello, Rest API!");
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
