import { z } from "zod";
import { prisma } from "../prisma";
import { t } from "../trpc";

export const userRouter = t.router({
  getAllUsers: t.procedure.query(async () => {
    try {
      const users = await prisma.user.findMany();
      return {
        status: "SUCCESS",
        message: "Utilisateurs récupérés !",
        users: users,
      };
    } catch (error) {
      return {
        status: "ERROR",
        message: "Problème lors de la récupération des données...",
        users: null,
      };
    }
  }),
  createUser: t.procedure
    .input(z.object({ username: z.string(), email: z.string().email() }))
    .mutation(async ({ input }) => {
      const user = await prisma.user.findFirst({
        where: {
          email: input.email,
        },
      });

      if (user) {
        return "Utilisateur existant...";
      } else {
        await prisma.user.create({
          data: {
            username: input.username,
            email: input.email,
          },
        });
        return "Utilisateur créé !";
      }
    }),
});
