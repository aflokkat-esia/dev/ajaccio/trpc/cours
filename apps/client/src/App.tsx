import UserForm from "./components/UserForm";
import { trpc } from "./utils/trpc";

function App() {
  const formatter = new Intl.DateTimeFormat("jp-jp", {
    dateStyle: "full",
  });

  const helloWorldQuery = trpc.hello.useQuery(undefined, {
    onSuccess(data) {
      console.log(data);
    },
  });
  const helloNameQuery = trpc.helloName.useQuery(
    { name: "Quentin" },
    {
      onError(err) {
        console.log(err);
      },
    }
  );

  const getAllUsersQuery = trpc.user.getAllUsers.useQuery();

  if (helloWorldQuery.isFetching) return <h1>Loading...</h1>;

  if (getAllUsersQuery.isFetching) return <h1>Loading...</h1>;

  return (
    <main>
      <h1>{helloWorldQuery.data}</h1>
      <h2>{helloNameQuery.data}</h2>
      <UserForm />
      {getAllUsersQuery.data?.users?.map((user) => (
        <p key={user.id}>
          {user.username} - {formatter.format(new Date(user.createdAt))}
        </p>
      ))}
    </main>
  );
}

export default App;
