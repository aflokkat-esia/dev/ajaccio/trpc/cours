import { useState } from "react";
import { trpc } from "../utils/trpc";

export default function UserForm() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");

  const utils = trpc.useContext();

  const createUserMutation = trpc.user.createUser.useMutation({
    onSuccess: (data) => {
      console.log(data);
      utils.user.getAllUsers.refetch();
    },
    onError: (err) => {
      console.log(err);
    },
  });

  return (
    <div>
      <input
        type="text"
        placeholder="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
      <input
        type="text"
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <button
        onClick={() => {
          if (email.includes("@")) {
            createUserMutation.mutate({
              username: username,
              email: email,
            });
          } else {
            console.log("Zeubi");
            setEmail("");
            setUsername("");
          }
        }}
      >
        Créer un utilisateur
      </button>
    </div>
  );
}
